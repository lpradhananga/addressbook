##Address Book 

An address book application

Requirement:

1. java 8
2. JUnit 5 for testing

Tests:

Please run the file src/test/java/AddressBookTest.java for test. 
Please note the dependencies for tests are in lib folder(src/test/lib).

Please note that user interface hasn't been implemented yet.