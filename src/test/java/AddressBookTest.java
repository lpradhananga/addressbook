package test.java;

import main.java.AddressBookHelper;
import main.java.model.ContactDetail;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Luna on 14/10/2018.
 */

@DisplayName("Address Book Test")
public class AddressBookTest {

    private  ContactDetail contactDetail1 = new ContactDetail( "John", "Smith",
            "+61420387522");
    private ContactDetail contactDetail2 = new ContactDetail("Luisa", "Sen",
            "+61420123521");
    private ContactDetail contactDetail3 = new ContactDetail("Lisa", "Bower",
            "+61434123999");

    @AfterEach
    void cleanAddressBook() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        addressBookHelper.resetAddressBooksMap();
    }

    @Test
    @DisplayName("Adding Contact Details to same address book")
    void addContactDetailsToSameAddressTest() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        addressBookHelper.addNewContactEntries("book1", contactDetail1);
        addressBookHelper.addNewContactEntries("book1", contactDetail2);
        assertEquals(addressBookHelper.getAddressBooksMap().keySet().size(), 1);
    }


    @Test
    @DisplayName("Adding duplicate Contact Details to same address book")
    void addDuplicateContactDetailsTest() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        addressBookHelper.addNewContactEntries("book1", contactDetail1);
        addressBookHelper.addNewContactEntries("book1", contactDetail1);
        assertEquals(addressBookHelper.getAddressBooksMap().keySet().size(), 1);
    }

    @Test
    @DisplayName("Adding Contact Details to same address book")
    void addContactDetailsToDifferentAddressTest() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        addressBookHelper.addNewContactEntries("book1", contactDetail1);
        addressBookHelper.addNewContactEntries("book2", contactDetail2);
        assertEquals(addressBookHelper.getAddressBooksMap().keySet().size(), 2);
    }

    @Test
    @DisplayName("Removing non existence contact Details")
    void removeNonExistenceContactDetails() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        String addressBookName = "book1";
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail1);
        addressBookHelper.removeContactEntries(addressBookName, contactDetail2);
        assertEquals(addressBookHelper.getContactDetails(addressBookName).size(), 1);
    }

    @Test
    @DisplayName("Removing existence contact Details")
    void removeExistenceContactDetails() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        String addressBookName = "book1";
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail1);
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail2);
        addressBookHelper.removeContactEntries(addressBookName, contactDetail2);
        assertEquals(addressBookHelper.getContactDetails(addressBookName).size(), 1);
    }

    @Test
    @DisplayName("Removing existence contact from wrong book")
    void removingFromWrongAddressBook() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        String addressBookName = "book1";
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail1);
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail2);
        addressBookHelper.removeContactEntries("book2", contactDetail2);
        assertEquals(addressBookHelper.getContactDetails(addressBookName).size(), 2);
    }

    @Test
    @DisplayName("Number of contact Details to same address book")
    void noContactDetailsToSameAddressTest() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        String addressBookName = "book1";
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail1);
        addressBookHelper.addNewContactEntries(addressBookName, contactDetail2);
        assertEquals(addressBookHelper.getContactDetails(addressBookName).size(), 2);
    }

    @Test
    @DisplayName("Number of unique contact Details to same address book")
    void noContactDetailsAllTest() {
        AddressBookHelper addressBookHelper = new AddressBookHelper();
        addressBookHelper.addNewContactEntries("book1", contactDetail1);
        addressBookHelper.addNewContactEntries("book1", contactDetail2);
        addressBookHelper.addNewContactEntries("book2", contactDetail2);
        addressBookHelper.addNewContactEntries("book3", contactDetail3);
        assertEquals(addressBookHelper.getAllContactDetails().size(), 3);
    }
}
