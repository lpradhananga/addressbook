package main.java.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Luna on 14/10/2018.
 */
public class AddressBook {

    private String name;
    private Set<ContactDetail> contactDetails;

    public AddressBook(String name) {
        this.name = name;
        this.contactDetails = new HashSet<ContactDetail>() {
        };
    }

    public String getName() {
        return name;
    }

    public Set<ContactDetail> getContactDetails() {
        return contactDetails;
    }

}
