package main.java;

import main.java.model.AddressBook;
import main.java.model.ContactDetail;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Luna on 14/10/2018.
 */
public class AddressBookHelper {

    private static Map<String, AddressBook> addressBooksMap= new HashMap<>();

    /**
     * get a map of address books
     * @return
     */
    public Map<String, AddressBook> getAddressBooksMap() {
        return addressBooksMap;
    }

    /**
     * clean the address book map
     * @return
     */
    public Map<String, AddressBook> resetAddressBooksMap() {
        return addressBooksMap = new HashMap<>();
    }

    /**
     * get contact details of a address book
     * @param addressBookName
     * @return
     */
    public Set<ContactDetail> getContactDetails(String addressBookName) {
        AddressBook addressBook =  addressBooksMap.get(addressBookName);
        if (addressBook == null || addressBook.getContactDetails() == null || addressBook.getContactDetails().isEmpty()) {
            return Collections.emptySet();
            }
        return addressBook.getContactDetails();
        }

    /**
     * add a new contact detail in a address book
     * @param addressBookName
     * @param contactDetail
     */
    public void addNewContactEntries(String addressBookName, ContactDetail contactDetail) {
       AddressBook addressBook =  addressBooksMap.get(addressBookName);
       if (addressBook == null) {
           addressBook = new AddressBook(addressBookName);
           addressBooksMap.put(addressBookName, addressBook);
       }
       Set<ContactDetail> contactDetailList = addressBook.getContactDetails();
       if (contactDetail != null) {
           contactDetailList.add(contactDetail);
       }
    }

    /**
     * remove contact detail from a address book
     * @param addressBookName
     * @param contactDetail
     */
    public void removeContactEntries(String addressBookName, ContactDetail contactDetail ) {
        AddressBook addressBook =  addressBooksMap.get(addressBookName);
        if (addressBook == null) {
            System.out.println(String.format("No contact details to remove from the address book name %s",
                    addressBookName));
            return;
        }
        Set<ContactDetail> contactDetailList = addressBook.getContactDetails();
        if (contactDetailList != null && !contactDetailList.isEmpty()) {
            if (contactDetail != null && contactDetailList.contains(contactDetail)) {
                contactDetailList.remove(contactDetail);
            }
        }
    }

    /**
     * print contact details from a address book
     * @param addressBookName
     */
    public void printContactsInAddressBook(String addressBookName) {
        AddressBook addressBook =  addressBooksMap.get(addressBookName);
        if (addressBook == null) {
            System.out.println(String.format("No such address book name %s", addressBookName));
        } else {
            Set<ContactDetail> contactDetailList = addressBook.getContactDetails();
            if (contactDetailList == null || contactDetailList.isEmpty()) {
                System.out.println("There is no contact entries to print");
            } else {
                contactDetailList.forEach(cd-> {
                    System.out.println(cd);
                });
            }
        }
    }

    /**
     * get unique set of contact details from all address books
     * @return
     */
    public Set<ContactDetail> getAllContactDetails() {
        return addressBooksMap.values().stream().map(a-> a.getContactDetails())
                .flatMap(Collection::stream).collect(Collectors.toSet());
    }

    /**
     * print unique set of contact details from all address books
     */
    public void printAllContactsInAddresBooks() {
        getAllContactDetails().forEach(cd-> {
            System.out.println(cd);
        });
    }
}
